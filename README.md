# Create duplicates of pages in a PDF

Uses Python PyPDF2 to split a PDF, duplicate pages and then combine back together.

![Example Screenshot](/inc/example.png)

## Installation

First install Python3 (for OSX use https://brew.sh/ as per screenshots below)

![Brew Install Screenshot](/inc/brew_install.png)

Install PyPDF2 using command `python3 -m pip install pypdf2`

![PyPDF2 Install Screenshot](/inc/py2pdf_install.png)

Copy the `split_and_merge.py` to a folder and rename the pdf file you want to process to `source.pdf`. Create a folder to hold the split files called `output`

![Folder Screenshot](/inc/folder.png)

Script defaults to splitting every 4 pages and making one additional duplicate (you can edit this in the script by changing the variables `step_page_length` and  `duplicate_quantity`)

## Running Script

Run the script from the command line using `python3 split_and_merge.py`.

Here is a video of the process: http://youtu.be/XiCftJnA_qo

## Note

Probably want to open the resulting `output.pdf` and preflight to your standards and resave as optimised PDF in Adobe Acrobat Professional before printing.

![Resave Screenshot](/inc/PressReady.png)

This has not been well tested. I have split and merged a simple 500 page document and I have split and merged a complex PDF to create a 2.5Gb output.pdf.

**Use at own risk.**