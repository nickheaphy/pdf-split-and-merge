# Quick hack.
# Splits a PDF and then combines
# Could do all in one step but not sure how much memory would be consumed.

# Untested - use at own risk.

from PyPDF2 import PdfFileWriter, PdfFileReader, PdfFileMerger
import os


# Configuration
step_page_length = 4
duplicate_quantity = 2

# Filenames
source_pdf = 'source.pdf'
output_pdf = 'output.pdf'



infile = PdfFileReader(open(source_pdf, 'rb'))

# Split
for i in range(0, infile.getNumPages(), step_page_length):
    outfile = PdfFileWriter()
    for k in range(duplicate_quantity):
        for j in range(i,i + step_page_length):
            p = infile.getPage(j)
            outfile.addPage(p)
    with open('output/page-%06d.pdf' % i, 'wb') as f:
        outfile.write(f)

# Merge
merger = PdfFileMerger()
files = [x for x in os.listdir('output') if x.endswith('.pdf')]
for fname in sorted(files):
    merger.append(PdfFileReader(open(os.path.join('output', fname), 'rb')))

merger.write(output_pdf)